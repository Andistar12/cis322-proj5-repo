# ACP Control Time Calculator

This project calculates control open and close times for a given route. This calculator is based on Randonneurs USA regulations, and more information can be found [here](https://rusa.org/pages/acp-brevet-control-times-calculator). 


## Test Cases

1st test case: no data
*  Wipe the database of all entries. This is done on server start
*  Immediately click "Display"
*  The display page should come up, with zero control point entries

2nd test case: valid data
*  Wipe the database of all entries. This is done on server start
*  Select 200km as the brevet length
*  Enter `120` or any valid integer between zero and 200 into the km input
*  Hit Submit. The text on the right should display "Submitted"
*  Hit Display. The display page should come up, with one control point entry

3rd test case: Invalid data
*  Select 200km as the brevet length
*  Enter alpha characters as km input, for example "asdf"
*  Hit Submit. The result should be that zero entries are submitted
*  Enter a negative value as km input
*  Hit Submit. The result should be that zero entries are submitted
*  Hit Display. The display page should come up, with zero control point entries

## Usage

First, set your brevet distance and start time. Then, for each desired control point, enter each distance in miles or kilometers, and the corresponding open and close times will be automatically calculated. 

## Assumptions

Calculations are based on rules specified by RUSA. This section explains some specifics about the implementation algorithm:

* For opening times, the control location is clamped to be between zero and the brevet distance
* For closing times, the control location is max'd to be at least zero. If the control point is farther than the brevet length, then it is assumed to be the end of the brevet, and separate calculations will occur instead
* Invalid data passed in will default to the default as specified in the API

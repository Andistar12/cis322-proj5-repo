"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import request, redirect, url_for
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
import json
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ["DB_ADDR"], 27017)
db = client.appdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/display.html")
def display():
    """
    Creates the display page from the entries in the database
    """
    app.logger.debug("Display page entry")
    brevet = request.args.get('brevet', 200, type=int)

    # Query database, then sort results
    query = db.ctimes.find({"brevet": brevet})
    query = sorted(query, key = lambda x: x["km"])

    data = f"{brevet}km ACP BREVET\n"
    data += "Checkpoint       Date  Time\n"
    data += "==========       ====  ====\n"
    for entry in query:
        open_time = arrow.get(entry["open"])
        close_time = arrow.get(entry["close"])

        data += "{:>5}km   start: {} {}\n          close: {} {}\n\n".format(
            entry["km"],
            open_time.format("MM-DD"), open_time.format("HH:mm"),
            close_time.format("MM-DD"), close_time.format("HH:mm"),
        )
    return flask.render_template("display.html", data=data)


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))

    km = request.args.get("km", "", type=str)

    try:
        km = float(km)
    except:
        km = None

    if km is not None and km >= 0:
        start = request.args.get('start', arrow.now().isoformat(), type=str)
        brevet = request.args.get('brevet', 200, type=int)
        open_time = acp_times.open_time(km, brevet, start)
        close_time = acp_times.close_time(km, brevet, start)

        times = {"open": open_time, "close": close_time}
        return flask.jsonify(result=times)
    else:
        result = {"error": "Please enter a valid non-negative integer"}
        return flask.jsonify(result=result)

@app.route("/_submit", methods=["GET", "POST"])
def _submit():
    """
    Submits the given data to 
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))

    brevet = request.args.get('brevet', 200, type=int)
    controls = request.args.get("controls", default=list())
    controls = json.loads(controls)
    result = {
        "result": "Unknown error occurred. No changes have been made"
    }
    if len(controls) == 0:
        result["result"] = "Zero entries submitted. No changes have been made"
    else:
        # Wipe previous entry
        db.ctimes.delete_many(dict())
        for entry in controls:
            data = {
                "km": entry["km"],
                "brevet": brevet,
                "open": entry["open"],
                "close": entry["close"]
            }
            # Insert entry
            db.ctimes.insert_one(data)
        result["result"] = f"{len(controls)} entries submitted"

    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    db.ctimes.delete_many(dict())
    app.run(port=CONFIG.PORT, host="0.0.0.0")
